import rules from "../assets/images/image-rules.svg";
type Props = {
  open: boolean;
  close: (key: boolean) => void;
};
const Rules = ({ open, close }: Props) => {
  if(!open)return
  return (
    <div className="rules-container">
      <div className="rules-header">
        <h1>Rules</h1>
      </div>
      <img src={rules} alt="" />
      <button onClick={() =>close(false)} className="btn-rules">X</button>
    </div>
  );
};

export default Rules;
