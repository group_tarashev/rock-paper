 // paper > rock > scissors > paper 0 > 1 > 2 > 0
export const result = (index:number, housePick:number) =>{
    if(index == housePick){
        return null
    }else if(index == 0 && housePick == 1){
        return true
    }else if(index == 1 && housePick == 2){
        return true
    }else if(index == 2 && housePick == 0){
        return true
    }
    return false
}