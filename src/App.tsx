import "./styles/App.css";
import rock from "./assets/images/icon-rock.svg";
import paper from "./assets/images/icon-paper.svg";
import scissors from "./assets/images/icon-scissors.svg";
import { useEffect, useState } from "react";
import { result } from "./utils/calculate.ts";
import Rules from "./components/Rules.tsx";
const images = [paper, rock, scissors]; // paper > rock > scissors > paper 0 > 1 > 2 > 0
function App() {
  const [index, setIndex] = useState<null | number>(null);
  const [housePick, setHousePick] = useState<null | number>(null);
  const [score, setScore] = useState(0);
  const [winner, setWinner] = useState<boolean | null>(null);
  const [rules, setRules] = useState(false);
  let rnd: number | null = null;
  useEffect(() => {
    if (index !== null) {
      rnd = Math.round(Math.random() * 2);
      setHousePick(rnd);
    }
    if (index !== null && housePick !== null) {
      console.log(result(index, housePick));
      const winner = result(index, housePick);
      setWinner(winner);
    }
  }, [index, housePick]);

  const reset = () => {
    if (winner) {
      setScore((c) => c + 1);
    } else if (winner == false) {
      setScore((c) => c - 1);
    }
    setIndex(null);
    setHousePick(null);
  };
  let winnerContent =
    winner === null ? (
      <p className="result">Equal</p>
    ) : winner ? (
      <p className="result">You win 1 score</p>
    ) : (
      <p className="result">You lost 1 score</p>
    );

  return (
    <div className="layout">
      <Rules open={rules} close={setRules}/>
      <div className="border">
        <ul className="list">
          <li>Rock</li>
          <li>Paper</li>
          <li>Scissors</li>
        </ul>
        <div className="score">
          <span>score</span>
          <span>{score}</span>
        </div>
      </div>
      <div className="container">
        {index !== null && <span className="u-pick">You picked</span>}
        {index !== null && <span className="house-pick">House picked</span>}
        {images.map((pic, i) => {
          if (index == null || index == i) {
            return (
              <button
                onClick={() => setIndex(i)}
                className={`btn btn-${i} ${index !== null && " move-center"}`}
                key={i}
              >
                <img src={pic} alt="" className="image" />
              </button>
            );
          }
        })}
        {housePick !== null && (
          <div className={`btn-house `}>
            <img
              src={images[housePick]}
              className={`btn btn-${housePick}`}
              alt="no pick"
            />
          </div>
        )}
        {housePick !== null &&<div className="house-shadow"></div>}
        <div className="result-cont">
          {housePick !== null && winnerContent}
          {index !== null && (
            <button onClick={() => reset()} className="reset">
              Next game
            </button>
          )}
        </div>
      </div>
      <button className="bnt-open-rules" onClick={() => setRules(true)}>Rules</button>
    </div>
  );
}

export default App;
